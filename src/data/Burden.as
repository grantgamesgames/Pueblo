
package data
{
	import flash.display.MovieClip;
	import flash.geom.ColorTransform;
	
	/**
	 * [Description]
	 *
	 * @author G$
	 * @since Mar 18, 2012
	 */
	public class Burden
	{
		
		//--------------------------------------
		// CONSTANTS
		//--------------------------------------
		public static const FRAME_END:String = "end";
		
		public static const BURDEN_WATER:uint = 0;
		public static const BURDEN_FOOD:uint = 1;
		
		private static const COLOR_DEFAULT:uint = 0xFFFFFF;
		public static const COLOR_WATER:uint = 0x0000FF;
		public static const COLOR_FOOD:uint = 0xFF0000;
		
		//--------------------------------------
		// VARIABLES
		//--------------------------------------
		public var color:uint = COLOR_DEFAULT;
		public var type:uint;
		public var value:uint;
		public var presentation:MovieClip;
		//--------------------------------------
		// CONSTRUCTOR
		//--------------------------------------
		public function Burden($type:uint, $value:uint, $presentation:MovieClip)
		{
			type = $type;
			value = $value;
			presentation = $presentation;
			switch($type)
			{
				case BURDEN_WATER :
					color = COLOR_WATER;
					break;
					
				case BURDEN_FOOD :
					color = COLOR_FOOD;
					break;
					
				default:
					break;
			}
		}
		
		//--------------------------------------
		// PROTECTED & PRIVATE METHODS
		//--------------------------------------	
		
		
		//--------------------------------------
		// PUBLIC METHODS
		//--------------------------------------
		
		
		//--------------------------------------
		// EVENT HANDLERS
		//--------------------------------------							
		
		
	}
}