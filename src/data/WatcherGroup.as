
package data
{
	
	/**
	 * [Description]
	 *
	 * @author G$
	 * @since May 6, 2012
	 */
	public class WatcherGroup
	{
		
		//--------------------------------------
		// CONSTANTS
		//--------------------------------------
		
		
		//--------------------------------------
		// VARIABLES
		//--------------------------------------
		public var nextTick:int = 0;
		public var lastTick:int;
		public var interval:uint;
		private var _watchers:Vector.<Watcher> = new <Watcher>[];
		
		//--------------------------------------
		// CONSTRUCTOR
		//--------------------------------------
		public function WatcherGroup($interval:uint)
		{
			interval = $interval;
		}
		
		//--------------------------------------
		// PROTECTED & PRIVATE METHODS
		//--------------------------------------	
		
		
		//--------------------------------------
		// PUBLIC METHODS
		//--------------------------------------
		public function onTick(dt:uint):void
		{
			if(nextTick < dt)
			{
				//tick
				nextTick = interval + nextTick - dt;
				lastTick += dt;

				var watcher:Watcher;
				for each(watcher in _watchers)
				{
					if(!watcher.paused)
					{
						if(watcher.repeat == -1)
							watcher.tick(lastTick);
						else if(watcher.repeat > 0)
						{
							watcher.tick(lastTick);
							watcher.repeat--;
						} else {
							watcher.complete(lastTick);
							_watchers.splice(_watchers.indexOf(watcher), 1);
						}
					}
				}
				lastTick = 0;
			} else {
				nextTick -= dt; //no tick
				lastTick += dt;
			}
		}
		
		public function addWatcher(newWatcher:Watcher):void
		{
			if(getWatcherByTick(newWatcher.tick) == null)
				_watchers.push(newWatcher);
		}
		
		public function removeWatcher(tick:Function):void
		{
			var watcher:Watcher = getWatcherByTick(tick);
			_watchers.splice(_watchers.indexOf(watcher), 1);
		}
		
		public function getWatcherByTick(tick:Function):Watcher
		{
			var watcher:Watcher;
			for each(watcher in _watchers)
				if(watcher.tick == tick)
					return watcher;
			return null;
		}
		
		//--------------------------------------
		// EVENT HANDLERS
		//--------------------------------------							
		
		
	}
}