
package data
{
	import flash.display.MovieClip;
	import flash.filters.GlowFilter;
	import flash.geom.ColorTransform;
	
	/**
	 * [Description]
	 *
	 * @author G$
	 * @since Mar 18, 2012
	 */
	public class FoodBlessing extends Blessing
	{
		
		//--------------------------------------
		// CONSTANTS
		//--------------------------------------
		private static const FOOD_COLOR:uint = 0xFFAAAA;
		private static const GLOW:GlowFilter = new GlowFilter(0xFF0000, 1, 5, 5, 1, 3);
		private static const FOOD_FILTERS:Array = [GLOW];
		
		//--------------------------------------
		// VARIABLES
		//--------------------------------------
		
		
		//--------------------------------------
		// CONSTRUCTOR
		//--------------------------------------
		public function FoodBlessing($presentation:Class)
		{
			super($presentation);
			filters = FOOD_FILTERS;
			color = FOOD_COLOR;
		}
		
		//--------------------------------------
		// PROTECTED & PRIVATE METHODS
		//--------------------------------------	
		
		
		//--------------------------------------
		// PUBLIC METHODS
		//--------------------------------------
		
		
		//--------------------------------------
		// EVENT HANDLERS
		//--------------------------------------							
		
		
	}
}