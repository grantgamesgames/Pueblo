
package data
{
	import flash.display.MovieClip;
	import flash.filters.GlowFilter;
	import flash.geom.ColorTransform;
	
	/**
	 * [Description]
	 *
	 * @author G$
	 * @since Mar 18, 2012
	 */
	public class CareBlessing extends Blessing
	{
		
		//--------------------------------------
		// CONSTANTS
		//--------------------------------------
		private static const CARE_COLOR:uint = 0xAAFFAA;
		private static const GLOW:GlowFilter = new GlowFilter(0x00FF00, 1, 5, 5, 1, 3);
		private static const CARE_FILTERS:Array = [GLOW];
		
		//--------------------------------------
		// VARIABLES
		//--------------------------------------
		
		
		//--------------------------------------
		// CONSTRUCTOR
		//--------------------------------------
		public function CareBlessing($presentation:Class)
		{
			super($presentation);
			filters = CARE_FILTERS;
			color = CARE_COLOR;
		}
		
		//--------------------------------------
		// PROTECTED & PRIVATE METHODS
		//--------------------------------------	
		
		
		//--------------------------------------
		// PUBLIC METHODS
		//--------------------------------------
		
		
		//--------------------------------------
		// EVENT HANDLERS
		//--------------------------------------							
		
		
	}
}