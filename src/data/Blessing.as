
package data
{
	import flash.display.MovieClip;
	import flash.filters.GlowFilter;
	import flash.geom.ColorTransform;
	
	/**
	 * [Description]
	 *
	 * @author G$
	 * @since Mar 18, 2012
	 */
	public class Blessing
	{
		
		//--------------------------------------
		// CONSTANTS
		//--------------------------------------
		private static const DEFAULT_COLOR:uint = 0xFFFFFF;
		private static const GLOW:GlowFilter = new GlowFilter(0xFFFFFF, 1, 5, 5, 1, 3);
		private static const DEFAULT_FILTERS:Array = [GLOW];
		
		//--------------------------------------
		// VARIABLES
		//--------------------------------------
		public var filters:Array = DEFAULT_FILTERS;
		public var color:uint = DEFAULT_COLOR;
		public var presentation:Class;
		
		//--------------------------------------
		// CONSTRUCTOR
		//--------------------------------------
		public function Blessing($presentation:Class)
		{
			presentation = $presentation;
			super();
		}
		
		//--------------------------------------
		// PROTECTED & PRIVATE METHODS
		//--------------------------------------	
		
		
		//--------------------------------------
		// PUBLIC METHODS
		//--------------------------------------
		
		
		//--------------------------------------
		// EVENT HANDLERS
		//--------------------------------------							
		
		
	}
}