
package
{
	import com.ghelton.GElement;
	import com.ghelton.WatchTower;
	
	import controllers.SpiritController;
	
	import data.CareBlessing;
	import data.Environment;
	import data.FoodBlessing;
	import data.Watcher;
	
	import elements.HeroSpirit;
	import elements.Kiva;
	import elements.Repository;
	import elements.Resource;
	import elements.Spirit;
	import elements.TargetElement;
	
	import events.FollowerControllerEvent;
	
	import flash.desktop.NativeApplication;
	import flash.desktop.SystemIdleMode;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	
	[SWF(width="1024", height="768", version_major="10", frameRate="25")]
	/**
	 * [Description]
	 *
	 * @author G$
	 * @since Mar 10, 2012
	 */
	public class Pueblo extends GElement
	{
		
		//--------------------------------------
		// CONSTANTS
		//--------------------------------------
		private static const DEBUG:Boolean = true;
		private static const SPIRITS:uint = 5;
		
		private static const COLOR_RED:uint = 0x712215;
		private static const COLOR_BLUE:uint = 0x032B5E;
		private static const COLOR_GREEN:uint = 0x27912F;
		
		private static const GAME_STATE_ACTIVE:uint = 0;
		private static const GAME_STATE_PAUSED:uint = 1;
		private static const GAME_STATE_ENDED:uint = 2;
		//--------------------------------------
		// VARIABLES
		//--------------------------------------
		private var _setupComplete:Boolean = false;
		private var _gameState:uint = GAME_STATE_ACTIVE;
		
		private var _presentation:Gameplay;
		private var _env:Environment;
		private var _temp:Sprite;
		private var _sky:TargetElement;
		
		private var _harvestKiva:TargetElement;
		private var _careKiva:TargetElement;
		
		private var _shelter:TargetElement;
		private var _lake:Resource;
		private var _huntingGrounds:Resource;
		
		private var _boredomSpots:Vector.<TargetElement>;
		private var _repositories:Vector.<Repository>;
		private var _spiritController:SpiritController;
		private var _infoPanels:Vector.<InfoPanel>;
		
		//--------------------------------------
		// CONSTRUCTOR
		//--------------------------------------
		public function Pueblo()
		{
			super();
			
			// support autoOrients
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			stage.addEventListener(Event.RESIZE, resizeStage);
			//somewhere in my init function 
			NativeApplication.nativeApplication.addEventListener(Event.ACTIVATE, handleActivate);
			NativeApplication.nativeApplication.addEventListener(Event.DEACTIVATE, handleDeactivate);
			NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.KEEP_AWAKE;
		}
		
		//--------------------------------------
		// PROTECTED & PRIVATE METHODS
		//--------------------------------------	
		
		private function setup():void
		{
			_env = new Environment();
			
			_presentation = new Gameplay();
//			_presentation.width = stage.stageWidth;
//			_presentation.height = stage.stageHeight;
//			_temp = new TargetElement();
			_shelter = new TargetElement(_presentation.shelter);
			var food:Repository = new Repository(_presentation.foodTable, Repository.REPOSITORY_FOOD, 100);
			_huntingGrounds = new Resource(_presentation.bush, OutsideBerry, food, Resource.RESOURCE_FOOD);
			_harvestKiva = new Kiva(_presentation.harvestKiva, _huntingGrounds, new FoodBlessing(OutsideBerry));
			_careKiva = new Kiva(_presentation.careKiva, _shelter, new CareBlessing());
			
			_boredomSpots = new <TargetElement>[food, _shelter];
			_repositories = new <Repository>[food];
			
			if(!DEBUG)
			{
				Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
				_presentation.harvestKiva.addEventListener(TouchEvent.TOUCH_TAP, onTouchFeed);
				_presentation.careKiva.addEventListener(TouchEvent.TOUCH_TAP, onTouchCare);
				_presentation.pause.addEventListener(TouchEvent.TOUCH_TAP, onTouchPause);
			} else {
				_presentation.harvestKiva.addEventListener(MouseEvent.CLICK, onClickFeed);
				_presentation.careKiva.addEventListener(MouseEvent.CLICK, onClickCare);
				_presentation.pause.addEventListener(MouseEvent.CLICK, onClickPause);
			}
			
//			addChild(_temp);
			addChild(_presentation);
			
			
/*			var matrix:Matrix = new Matrix();
			matrix.createGradientBox(stage.stageWidth, stage.stageHeight, (Math.PI/180)*90, 0, 00);
			_sky.graphics.beginGradientFill(GradientType.LINEAR, [0x031881, 0x5AA1E1], [1, 1], [0,255], matrix);
			_sky.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
			_sky.graphics.endFill();
			
			_temp.graphics.beginGradientFill(GradientType.LINEAR, [0x000000, 0x000000], [1, 0], [0,100], matrix);
			_temp.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
			_temp.graphics.endFill();
*/			
		
			//Info Panels
			_infoPanels = new <InfoPanel>[];;
			var panel:InfoPanel
			//Spirits
			_spiritController = new SpiritController();
			_spiritController.addEventListener(FollowerControllerEvent.ALL_FOLLOWER_INACTIVE, onAllSpiritsDead);
			var spirit:HeroSpirit;
			for(var j:uint = 0; j < SPIRITS; j++)
			{
				spirit = new HeroSpirit(_shelter, new Hero());
				spirit.name = "Spirit " + j;
				_spiritController.addSpirit(spirit);
				_presentation.characterLoadpoint.addChild(spirit);
				
				panel = new InfoPanel();
				_infoPanels.push(panel);
				_presentation.characterLoadpoint.addChild(panel);
				panel.x = j * (panel.width + 10);
				panel.y = 20;
			}
			
			WatchTower.addWatcher(new Watcher(WatchTower.FRAME_TIME, onFrame));
			WatchTower.addWatcher(new Watcher(Environment.DAY_TIME, onDay));
			WatchTower.startWatching(stage);
		}
		
		private function onFrame(dt:uint):void
		{
			_spiritController.moveFollowers(_boredomSpots);
			var emptyRepos:uint = 0;
			for each(var repo:Repository in _repositories)
			{
				if(repo.value == 0)
					emptyRepos++;
			}
		}
		
		private function onDay(dt:uint):void
		{
			var panel:InfoPanel;
			for( var i:int = 0; i < _infoPanels.length; i++)
			{
				_infoPanels[i].strength.text = _spiritController.spirits[i].strength.toString();
				_infoPanels[i].health.text = _spiritController.spirits[i].health.toString();
				_infoPanels[i].hunger.text = _spiritController.spirits[i].hunger.toString();
			}
		}
		
		private function endGame():void
		{
			handleDeactivate(null);
			_gameState = GAME_STATE_ENDED;
			var endGame:Sprite = new Sprite();
			endGame.graphics.beginFill(0, 0.8);
			endGame.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
			endGame.graphics.endFill();
			endGame.mouseChildren = false;
			if(!DEBUG)
			{
				endGame.addEventListener(TouchEvent.TOUCH_TAP, onEndGameTap);
			} else {
				endGame.addEventListener(MouseEvent.CLICK, onEndGameTap);
			}
			addChild(endGame);
		}
		
		private function resizeStage(e:Event):void
		{
			if(!_setupComplete && stage.stageWidth > stage.stageHeight)
			{
				setup();
				_setupComplete = true;
			}
		}
		
		
		private function onFeed():void
		{
			if(_spiritController.requestFollower(_huntingGrounds))
				trace("FEED");
		}
		
		private function onCare():void
		{
			if(_spiritController.requestFollower(_shelter))
				trace("CARE");
		}
		
		//--------------------------------------
		// PUBLIC METHODS
		//--------------------------------------
		
		
		//--------------------------------------
		// EVENT HANDLERS
		//--------------------------------------	
		private function onEndGameTap(e:Event):void
		{
			var spirit:Spirit;
			_spiritController.rebirth();
			var repo:Repository;
			for each(repo in _repositories)
				repo.rebirth();
			removeChild(e.target as DisplayObject);
			_gameState = GAME_STATE_PAUSED;
			handleActivate(e);
		}
		
		private function resumeGame():void
		{
			if(!stage.hasEventListener(Event.ENTER_FRAME) && _gameState == (GAME_STATE_ACTIVE | GAME_STATE_PAUSED))
			{
				_gameState = GAME_STATE_ACTIVE;
				WatchTower.resumeWatch(stage);
				while(_presentation.menuLoadpoint.numChildren > 0)
					_presentation.menuLoadpoint.removeChildAt(0);
			}	
		}
		
		private function pauseGame():void
		{
			if(stage.hasEventListener(Event.ENTER_FRAME) && _gameState == GAME_STATE_ACTIVE)
			{
				WatchTower.pauseWatching(stage);
				_gameState = GAME_STATE_PAUSED;
				_presentation.menuLoadpoint.addChild(new PauseMenu());
			}
		}
		
		private function togglePause():void
		{
			if(_gameState == GAME_STATE_ACTIVE)
			{
				pauseGame();
				_presentation.pause.gotoAndStop(2);
			} else {
				resumeGame();
				_presentation.pause.gotoAndStop(1);
			}
		}
		
		private function handleActivate(e:Event):void
		{
			resumeGame();
		}
		
		private function handleDeactivate(e:Event):void
		{
			pauseGame();
		}
		
		private function onTouchFeed(e:TouchEvent):void
		{
			onFeed();
		}
		
		private function onTouchCare(e:TouchEvent):void
		{
			onCare();
		}
		
		private function onClickFeed(e:MouseEvent):void
		{
			onFeed();
		}
		
		private function onClickCare(e:MouseEvent):void
		{
			onCare();
		}
		
		private function onAllSpiritsDead(e:FollowerControllerEvent):void
		{
			endGame();
		}
		
		private function onTouchPause(e:TouchEvent):void
		{
			togglePause();
		}
		
		private function onClickPause(e:MouseEvent):void
		{
			togglePause();
		}
		
	}
}