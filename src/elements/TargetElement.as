
package elements
{
	import com.ghelton.GElement;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.geom.Point;
	
	
	/**
	 * [Description]
	 *
	 * @author G$
	 * @since Mar 11, 2012
	 */
	public class TargetElement
	{
		
		//--------------------------------------
		// CONSTANTS
		//--------------------------------------
		
		
		//--------------------------------------
		// VARIABLES
		//--------------------------------------
		protected var _element:DisplayObject;
		
		//--------------------------------------
		// CONSTRUCTOR
		//--------------------------------------
		public function TargetElement($presentation:DisplayObject)
		{
			super();
			_element = $presentation;
		}
		
		//--------------------------------------
		// PROTECTED & PRIVATE METHODS
		//--------------------------------------	
		
		//--------------------------------------
		// PUBLIC METHODS
		//--------------------------------------
		public function getNextTarget():TargetElement
		{
			return null;
		}
		
		public function get position():Point
		{
			return _element.localToGlobal(new Point(_element.stage.x, _element.stage.y));
		}
		//--------------------------------------
		// EVENT HANDLERS
		//--------------------------------------							
		
		
	}
}