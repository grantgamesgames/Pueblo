
package elements
{
	import com.ghelton.GElement;
	
	import data.Watcher;
	
	import events.FollowerEvent;
	
	import flash.geom.Point;
	
	/**
	 * [Description]
	 *
	 * @author G$
	 * @since Mar 17, 2012
	 */
	public class Follower extends GElement
	{
		
		//--------------------------------------
		// CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		// VARIABLES
		//--------------------------------------
		protected var _speed:uint = 3;
		private var _location:Point;
		public var currentTarget:TargetElement;
		protected var _birthplace:TargetElement;
		
		//--------------------------------------
		// CONSTRUCTOR
		//--------------------------------------
		public function Follower(birthplace:TargetElement)
		{
			super();
			_birthplace = birthplace;
			this.x = birthplace.position.x;
			this.y = birthplace.position.y;
			_location = new Point(birthplace.position.x, birthplace.position.y);
		}
		
		//--------------------------------------
		// PROTECTED & PRIVATE METHODS
		//--------------------------------------	
		protected function foundTarget():void
		{
			currentTarget = currentTarget.getNextTarget();
			dispatchEvent(new FollowerEvent(FollowerEvent.FOUND_TARGET));
		}
		
		public function get rested():Boolean
		{
			return true;
		}
		//--------------------------------------
		// PUBLIC METHODS
		//--------------------------------------
		
		public function move():Boolean
		{
			if(currentTarget != null)
			{
				var targetPoint:Point = new Point(currentTarget.position.x, currentTarget.position.y);
				var xDiff:Number = Math.abs(_location.x - targetPoint.x);
				var yDiff:Number = Math.abs(_location.y - targetPoint.y);
				if(xDiff < _speed && yDiff < _speed)
				{
					foundTarget();
				} else {
					var newPos:Point = targetPoint.subtract(_location);
					newPos.normalize( _speed );
					this.x += newPos.x;
					this.y += newPos.y;
					_location.x = this.x;
					_location.y = this.y;
				}
			}
			return true;//moved
		}
		
		public function rebirth():void
		{
			currentTarget = null;
			this.x = _birthplace.position.x;
			this.y = _birthplace.position.y;
			_location = _birthplace.position.clone();
			redraw();
		}
		//--------------------------------------
		// EVENT HANDLERS
		//--------------------------------------							
	}
}