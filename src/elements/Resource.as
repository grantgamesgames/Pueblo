
package elements
{
	import data.Burden;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.geom.Point;
	
	/**
	 * [Description]
	 *
	 * @author G$
	 * @since Mar 18, 2012
	 */
	public class Resource extends TargetElement
	{
		
		//--------------------------------------
		// CONSTANTS
		//--------------------------------------
		public static const RESOURCE_WATER:uint = 0;
		public static const RESOURCE_FOOD:uint = 1;
		
		private static const COLOR_RED:uint = 0x712215;
		private static const COLOR_BLUE:uint = 0x032B5E;
		private static const COLOR_GREEN:uint = 0x27912F;
		private static const MAX_VALUE:uint = 10000;
		
		//--------------------------------------
		// VARIABLES
		//--------------------------------------
		private var _repository:TargetElement;
		private var _type:uint;
		private var _value:uint = MAX_VALUE;
		private var _presentation:MovieClip;
		private var _burdenPresentation:Class;
		//--------------------------------------
		// CONSTRUCTOR
		//--------------------------------------
		public function Resource($presentation:MovieClip, $burdenPresentation:Class, repository:TargetElement, $resourceType:uint)
		{
			super($presentation);
			_repository = repository;
			_type = $resourceType;
			_presentation = $presentation;
			_burdenPresentation = $burdenPresentation;
			
			for(var i:uint = 0; i < 40; i++)
				addBerry(true);
		}
		
		//--------------------------------------
		// PROTECTED & PRIVATE METHODS
		//--------------------------------------	

		private function addBerry(randomSize:Boolean = false):void
		{
			var berry:MovieClip = new _burdenPresentation();
			var startFrame:uint = 0;
			if(randomSize)
				startFrame = Math.random() * berry.totalFrames;
			_presentation.addChild(berry);
//			trace("_presentation.height:",_presentation.height);
			berry.x = Math.max(berry.width, (Math.random() * _presentation.width - berry.width));
			berry.y = Math.max(berry.height, (Math.random() * _presentation.height - berry.height));
//			trace("x:",berry.x,"y:",berry.y);
			berry.gotoAndPlay(startFrame);
		}
		//--------------------------------------
		// PUBLIC METHODS
		//--------------------------------------
		override public function get position():Point
		{
			return _presentation.localToGlobal(new Point(_presentation.getChildAt(0).x, _presentation.height + 20));
		}
		
		override public function getNextTarget():TargetElement
		{
			if(_repository)
				return _repository;
			else
				return super.getNextTarget();
		}
		
		public function getBurden(strength:uint):Burden
		{
			if(strength > _value) {
				_value = 0;
			} else {
				_value -= strength;
			}
//			_presentation.gotoAndPlay();
			_presentation.removeChildAt(0);
			addBerry();
			return new Burden(_type, strength, new _burdenPresentation());
		}
		
		//--------------------------------------
		// EVENT HANDLERS
		//--------------------------------------							
		
		
	}
}