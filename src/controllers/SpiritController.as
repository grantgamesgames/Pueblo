
package controllers
{
	import com.ghelton.WatchTower;
	
	import elements.Follower;
	import elements.Spirit;
	
	import events.SpiritEvent;
	
	/**
	 * [Description]
	 *
	 * @author G$
	 * @since Apr 9, 2012
	 */
	public class SpiritController extends FollowerController
	{
		
		//--------------------------------------
		// CONSTANTS
		//--------------------------------------
		
		
		//--------------------------------------
		// VARIABLES
		//--------------------------------------
		
		//--------------------------------------
		// CONSTRUCTOR
		//--------------------------------------
		public function SpiritController()
		{
			super();
		}
		
		//--------------------------------------
		// PROTECTED & PRIVATE METHODS
		//--------------------------------------	
		private function onDeath(e:SpiritEvent):void
		{
			deactivateFollower(e.spirit);
		}
		
		//--------------------------------------
		// PUBLIC METHODS
		//--------------------------------------
		public function addSpirit(spirit:Spirit):void 
		{
			if(!spirit.hasEventListener(SpiritEvent.DEATH))
				spirit.addEventListener(SpiritEvent.DEATH, onDeath);
			super.addFollower(spirit);
		}
		
		override public function addFollower(follower:Follower):void
		{
			addSpirit(follower as Spirit);
		}
		
		public function get spirits():Vector.<Spirit>
		{
			var spirits:Vector.<Spirit> = new <Spirit>[];
			var spirit:Spirit;
			for each(spirit in _followers)
				spirits.push(spirit);
			return spirits;
		}
		
		
		//--------------------------------------
		// EVENT HANDLERS
		//--------------------------------------							
		
		
	}
}